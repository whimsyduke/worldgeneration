﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace WorldGeneration
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (System.IO.File.Exists("code.txt"))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader("code.txt");
                Script.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        // 各Cube类型边长
        static double[][] side = new double[9][]
        {
                new double [2] { 16, 16 },
                new double [2] { 32, 32 },
                new double [2] { 64, 64 },
                new double [2] { 32, 16 },
                new double [2] { 16, 32 },
                new double [2] { 64, 16 },
                new double [2] { 16, 64 },
                new double [2] { 64, 32 },
                new double [2] { 32, 64 }
        };
        static Color[] color = { Color.FromRgb(153, 153, 153), Color.FromRgb(255, 0, 0), Color.FromRgb(255, 127, 0), Color.FromRgb(0, 127, 255), Color.FromRgb(0, 255, 63), Color.FromRgb(133, 59, 135), Color.FromRgb(127, 255, 255) };
        System.Drawing.Bitmap bitmap;
        static Color[] acColor = {Colors.AliceBlue,Colors.Aqua,Colors.Aquamarine,Colors.Azure,Colors.Beige,Colors.Bisque,Colors.BlanchedAlmond,Colors.Blue,Colors.BlueViolet,Colors.Brown,Colors.BurlyWood,Colors.CadetBlue,Colors.Chartreuse,
            Colors.Chocolate,Colors.Coral,Colors.CornflowerBlue,Colors.Cornsilk,Colors.Crimson,Colors.Cyan,Colors.DarkBlue,Colors.DarkCyan,Colors.DarkGoldenrod,Colors.DarkGray,Colors.DarkGreen,Colors.DarkKhaki,Colors.DarkMagenta,Colors.DarkOliveGreen,
            Colors.DarkOrange,Colors.DarkOrchid,Colors.DarkRed,Colors.DarkSalmon,Colors.DarkSeaGreen,Colors.DarkSlateBlue,Colors.DarkSlateGray,Colors.DarkTurquoise,Colors.DarkViolet,Colors.DeepPink,Colors.DeepSkyBlue,Colors.DimGray,
            Colors.DodgerBlue,Colors.Firebrick,Colors.ForestGreen,Colors.Fuchsia,Colors.Gainsboro,Colors.Gold,Colors.Goldenrod,Colors.Gray,Colors.Green,Colors.GreenYellow,Colors.Honeydew,Colors.HotPink,Colors.IndianRed,
            Colors.Indigo,Colors.Ivory,Colors.Khaki,Colors.Lavender,Colors.LavenderBlush,Colors.LawnGreen,Colors.LemonChiffon,Colors.LightBlue,Colors.LightCoral,Colors.LightCyan,Colors.LightGoldenrodYellow,Colors.LightGray,Colors.LightGreen,
            Colors.LightPink,Colors.LightSalmon,Colors.LightSeaGreen,Colors.LightSkyBlue,Colors.LightSlateGray,Colors.LightSteelBlue,Colors.LightYellow,Colors.Lime,Colors.LimeGreen,Colors.Linen,Colors.Magenta,Colors.Maroon,Colors.MediumAquamarine,
            Colors.MediumBlue,Colors.MediumOrchid,Colors.MediumPurple,Colors.MediumSeaGreen,Colors.MediumSlateBlue,Colors.MediumSpringGreen,Colors.MediumTurquoise,Colors.MediumVioletRed,Colors.MidnightBlue,Colors.MintCream,
            Colors.MistyRose,Colors.Moccasin,Colors.Navy,Colors.OldLace,Colors.Olive,Colors.OliveDrab,Colors.Orange,Colors.OrangeRed,Colors.Orchid,Colors.PaleGoldenrod,Colors.PaleGreen,Colors.PaleTurquoise,Colors.PaleVioletRed,
            Colors.PapayaWhip,Colors.PeachPuff,Colors.Peru,Colors.Pink,Colors.Plum,Colors.PowderBlue,Colors.Purple,Colors.Red,Colors.RosyBrown,Colors.RoyalBlue,Colors.SaddleBrown,Colors.Salmon,Colors.SandyBrown,Colors.SeaGreen,Colors.SeaShell,Colors.Sienna,
            Colors.Silver,Colors.SkyBlue,Colors.SlateBlue,Colors.SlateGray,Colors.Snow,Colors.SpringGreen,Colors.SteelBlue,Colors.Tan,Colors.Teal,Colors.Thistle,Colors.Tomato,Colors.Transparent,Colors.Turquoise,Colors.Violet,Colors.Wheat,Colors.WhiteSmoke,
            Colors.Yellow,Colors.YellowGreen};
        System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
        static Regex CubeUse_Name = new Regex("(?<=lib2747C076_gf_CubeInitZhuCe\\(StringExternal\\(\\\"Param/Value/\\w+\\\"\\), \\\")\\w+(?=\\\", StringExternal\\()");
        static Regex CubeUse_CubeInitDoorSizeX = new Regex("(?<=lib2747C076_gf_CubeInitDoor\\(((false|true),){12})[-+]?[\\d]+(?=,[-+]?[\\d]+\\);)");
        static Regex CubeUse_CubeInitDoorSizeY = new Regex("(?<=lib2747C076_gf_CubeInitDoor\\(((false|true),){12}[-+]?[\\d]+,)[-+]?[\\d]+(?=\\);)");
        static Regex CubeUse_CreateHeight = new Regex("lib2747C076_gf_CreateHeight\\([-+]?[\\d]{1,2},[-+]?[\\d]{1,2},\\\"([-+]?\\d+(\\.\\d+)? )*[-+]?\\d+(\\.\\d+)?\\\"\\);");
        static Regex CubeUse_HeightStr = new Regex("(?<=lib2747C076_gf_CreateHeight\\([-+]?[\\d]{1,2},[-+]?[\\d]{1,2},\\\")([-+]?\\d+(\\.\\d+)? )*[-+]?\\d+(\\.\\d+)?(?=\\\"\\);)");
        static Regex CubeUse_CreateZuYin = new Regex("lib2747C076_gf_CreateZuYin\\(([-+]?[\\d]+,){5}[-+]?[\\d]+\\);");
        static Regex CubeUse_ZuYinStr = new Regex("(?<=lib2747C076_gf_CreateZuYin\\()([-+]?[\\d]+,){5}[-+]?[\\d]+(?=\\);)");

        static Regex CreateForm_CreateCube = new Regex("lib2747C076_gf_WHTest_CreateRandomForm\\(([\\d]+,){4}((false|true),){11}(false|true)\\);");
        static Regex CreateForm_CubeRect = new Regex("(?<=lib2747C076_gf_WHTest_CreateRandomForm\\()([-+]?[\\d]+,){3}[\\d]+(?=,((false|true),){11}(false|true)\\);)");
        static Regex CreateForm_GateStatus = new Regex("(?<=lib2747C076_gf_WHTest_CreateRandomForm\\(([-+]?[\\d]+,){4})((false|true),){11}(false|true)(?=\\);)");

        static Regex FormLog_CubeGenerationIndex = new Regex("^[-+]?[\\d]+");
        static Regex FormLog_CubePlace = new Regex("GenerationCube:([-+]?[\\d]+,){3}[-+]?[\\d]+\\r?\\n?(GateOfCube:([-+]?[\\d]+,){4}[-+]?[\\d]+\\r?\\n?)+");

        class FormItem : ListBoxItem
        {
            public Rectangle RectLink { get; set; }
            public int[] CubeRectValue { get; set; }
            public bool[] GateStatus { get; set; }
            public int Index { get; set; }
            public FormItem(string name, int index, Rectangle rect, int[] cubeRectValue, bool[] gateStatus)
            {
                Content = name;
                RectLink = rect;
                CubeRectValue = cubeRectValue;
                GateStatus = gateStatus;
                Index = index;
            }
        }
        class GenerationFunctionItem : ListBoxItem
        {
            public List<PlaceCubeItem> PlaceCubes = new List<PlaceCubeItem>();
            public GenerationFunctionItem(string name)
            {
                Content = name;
                Selected += GenerationFunctionItem_Selected;
            }

            private void GenerationFunctionItem_Selected(object sender, RoutedEventArgs e)
            {
                MainWindow main = GetWindow(sender as DependencyObject) as MainWindow;
                main.PlaceCubeList.Items.Clear();
                foreach (PlaceCubeItem select in PlaceCubes)
                {
                    main.PlaceCubeList.Items.Add(select);
                }
            }
        }
        class PlaceCubeItem : ListBoxItem
        {
            public bool IsPlace;
            public int Index;
            public Cube PlaceCubeSharp;
            public PlaceCubeItem(string name, int index, Cube place, bool isPlace)
            {
                Content = name;
                Index = index;
                PlaceCubeSharp = place;
                IsPlace = isPlace;
                Selected += PlaceCubeItem_Selected;
            }

            private void PlaceCubeItem_Selected(object sender, RoutedEventArgs e)
            {
                MainWindow main = GetWindow(sender as DependencyObject) as MainWindow;
                PlaceCubeItem cube = sender as PlaceCubeItem;
                if (cube == null) return;
                foreach (PlaceCubeItem select in main.alllaceCubeList)
                {
                    if (select.Index > cube.Index)
                    {
                        select.PlaceCubeSharp.SetVisible(Visibility.Collapsed);
                    }
                    else
                    {
                        select.PlaceCubeSharp.SetVisible(Visibility.Visible);
                    }
                }
            }
        }
        class Cube
        {
            public Path CubeSharp;
            List<Gate> GateList = new List<Gate>();
            public Cube(MainWindow main, string cubeStr, string[] gateStrs)
            {
                int[] cubeValue = cubeStr.Replace("GenerationCube:", "").Split(new char[] { ',' }).Select(r => int.Parse(r)).ToArray();

                Path p = new Path();
                p.Fill = new SolidColorBrush(Colors.Green);
                p.Stroke = new SolidColorBrush(Colors.Black);
                p.StrokeThickness = 1;
                p.Width = 512;
                p.Height = 512;
                p.SetValue(Canvas.LeftProperty, 0.0);
                p.SetValue(Canvas.BottomProperty, 0.0);
                p.Data = new RectangleGeometry(new Rect(cubeValue[2] * 2 - 2 - side[cubeValue[1]][0], 514 - cubeValue[3] * 2 - side[cubeValue[1]][1], side[cubeValue[1]][0] * 2, side[cubeValue[1]][1] * 2));
                CubeSharp = p;
                main.WorldPanel.Children.Add(p);

                foreach (string select in gateStrs)
                {
                    int[] gateValue = select.Replace("GateOfCube:", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(r => int.Parse(r)).ToArray();
                    Gate g = new Gate(gateValue[0], gateValue[1], gateValue[2], gateValue[3], gateValue[4]);
                    GateList.Add(g);
                    main.WorldPanel.Children.Add(g.GateSharp);
                }
            }
            public Cube(MainWindow main, double x, double y, double width, double hight, bool[] lp_GateOpen)
            {

                Path p = new Path();
                p.Fill = new SolidColorBrush(Colors.Green);
                p.Stroke = new SolidColorBrush(Colors.Black);
                p.StrokeThickness = 1;
                p.Width = 512;
                p.Height = 512;
                p.SetValue(Canvas.LeftProperty, 0.0);
                p.SetValue(Canvas.BottomProperty, 0.0);
                p.Data = new RectangleGeometry(new Rect(x * 2 - 2 - width, 514 - y * 2 - hight, width * 2, hight * 2));
                CubeSharp = p;
                main.WorldPanel.Children.Add(p);


            }
            public void SetVisible(System.Windows.Visibility vis)
            {
                CubeSharp.Visibility = vis;
                foreach (Gate select in GateList)
                {
                    select.GateSharp.Visibility = vis;
                }
            }
        }

        class Gate
        {
            public int GateIndex;
            public int Type;
            public double X;
            public double Y;
            public int LinkCube;
            public Path GateSharp;
            public Gate(int gateIndex, int type, int x, int y, int linkCube)
            {
                GateIndex = gateIndex;
                Type = type;
                X = x;
                Y = y;
                LinkCube = linkCube;
                Path p = new Path();
                p.Fill = new SolidColorBrush(Colors.Black);
                p.StrokeThickness = 0;
                p.Width = 512;
                p.Height = 512;
                p.SetValue(Canvas.LeftProperty, 0.0);
                p.SetValue(Canvas.BottomProperty, 0.0);
                p.Data = new EllipseGeometry(new Point(x * 2 - 2, 514 - y * 2), 3, 3);
                GateSharp = p;
            }
        }

        List<PlaceCubeItem> alllaceCubeList = new List<PlaceCubeItem>();
        private void Generation_Click(object sender, RoutedEventArgs e)
        {
            WorldPanel.Children.Clear();
            int i;
            CubeList.Items.Clear();
            alllaceCubeList.Clear();
            PlaceCubeList.Items.Clear();
            //try
            //{
            switch (SoftwareMode.SelectedIndex)
            {
                case 0:
                    WorldPanelBox.Header = "世界界面（地图范围：256×256）";
                    string[] CubeGenLogs = Script.Text.Split(new string[] { "GenerationCubeFunction:" }, StringSplitOptions.RemoveEmptyEntries);
                    i = 0;
                    foreach (var select in CubeGenLogs)
                    {
                        string genIndexStr = FormLog_CubeGenerationIndex.Match(select.ToString()).ToString();
                        int genIndex = int.Parse(genIndexStr);
                        GenerationFunctionItem geneItem = new GenerationFunctionItem("GenerationNewCube:" + genIndexStr);
                        CubeList.Items.Add(geneItem);
                        var placeStrs = FormLog_CubePlace.Matches(select.ToString());
                        foreach (var placeStr in placeStrs)
                        {
                            string cubeInfo = placeStr.ToString().Substring(0, placeStr.ToString().IndexOf('\n')).Replace("\r", "");
                            string[] gateInfos = placeStr.ToString().Replace(cubeInfo, "").Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                            PlaceCubeItem placeItem = new PlaceCubeItem(cubeInfo, i, new Cube(this, cubeInfo, gateInfos), true);
                            alllaceCubeList.Add(placeItem);
                            geneItem.PlaceCubes.Add(placeItem);
                            i++;
                        }
                    }
                    break;
                case 1:
                    WorldPanelBox.Header = "世界界面（地图范围：256×256）";
                    var Cubes = CreateForm_CreateCube.Matches(Script.Text);
                    i = 0;
                    foreach (var select in Cubes)
                    {
                        string RectName = CreateForm_CubeRect.Match(select.ToString()).Value;
                        int[] CubeRectValue = CreateForm_CubeRect.Match(select.ToString()).Value.Split(new char[] { ',' }).Select(r => int.Parse(r)).ToArray();
                        bool[] GateStatus = CreateForm_GateStatus.Match(select.ToString()).Value.Split(new char[] { ',' }).Select(r => r == "true").ToArray();
                        Rectangle rect = new Rectangle();
                        rect.Fill = new SolidColorBrush(acColor[i]);
                        i++;
                        if (i >= 135) i = 0;
                        rect.Stroke = new SolidColorBrush(Colors.Black);
                        rect.Width = CubeRectValue[2] * 2 + 2;
                        rect.Height = CubeRectValue[3] * 2 + 2;
                        rect.SetValue(Canvas.LeftProperty, (double)(CubeRectValue[0]) * 2 - rect.Width / 2);
                        rect.SetValue(Canvas.BottomProperty, (double)(CubeRectValue[1]) * 2 - rect.Height / 2);
                        rect.StrokeThickness = 1;
                        WorldPanel.Children.Add(rect);
                        FormItem item = new FormItem("Index:" + i.ToString() + " : " + RectName, i, rect, CubeRectValue, GateStatus);
                        item.Selected += CubeItem_Selected;
                        CubeList.Items.Add(item);
                    }
                    break;
                case 2:
                    string name = CubeUse_Name.Match(Script.Text).ToString();
                    var Heights = CubeUse_CreateHeight.Matches(Script.Text);
                    var ZuYins = CubeUse_CreateZuYin.Matches(Script.Text);
                    if (CubeUse_CubeInitDoorSizeX.Match(Script.Text).ToString() == "") return;
                    int CubeSizeX = int.Parse(CubeUse_CubeInitDoorSizeX.Match(Script.Text).ToString());
                    int CubeSizeY = int.Parse(CubeUse_CubeInitDoorSizeY.Match(Script.Text).ToString());
                    int CubeSizeMax = CubeSizeX > CubeSizeY ? CubeSizeX : CubeSizeY;
                    WorldPanelBox.Header = "世界界面（总分辨率：" + CubeSizeMax * 32 + "×" + CubeSizeMax * 32 + "，生成分辨率：" + CubeSizeX * 32 + "×" + CubeSizeY * 32 + "）";
                    double[,] CubeHight = new double[CubeSizeY, CubeSizeX];
                    bitmap = new System.Drawing.Bitmap(CubeSizeX * 32 + 32, CubeSizeY * 32);
                    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
                    //设置高质量插值法
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                    //设置高质量,低速度呈现平滑程度
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    //清空画布并以透明背景色填充
                    g.Clear(System.Drawing.Color.Black);
                    i = 0;
                    foreach (var select in Heights)
                    {
                        string[] hightStr = CubeUse_HeightStr.Match(select.ToString()).Value.Split(new char[] { ' ' });
                        int j = 0;
                        foreach (string str in hightStr)
                        {
                            CubeHight[i, j] = double.Parse(str);
                            j++;
                        }
                        i++;
                    }

                    Path backgroundPath = new Path();
                    WorldPanel.Children.Add(backgroundPath);
                    backgroundPath.Data = new GeometryGroup();
                    backgroundPath.Fill = new SolidColorBrush(Colors.Black);
                    backgroundPath.StrokeThickness = 0;
                    backgroundPath.Width = 512;
                    backgroundPath.Height = 512;
                    backgroundPath.SetValue(Canvas.LeftProperty, 0.0);
                    backgroundPath.SetValue(Canvas.BottomProperty, 0.0);
                    RectangleGeometry background;
                    background = new RectangleGeometry(new Rect(0, 0, CubeSizeX * 512 / CubeSizeMax, CubeSizeY * 512 / CubeSizeMax));
                    (backgroundPath.Data as GeometryGroup).Children.Add(background);

                    Path[] path = { new Path(), new Path(), new Path(), new Path(), new Path(), new Path(), new Path(), new Path(), };
                    GeometryGroup gg = new GeometryGroup();
                    int type = 0;
                    foreach (var select in ZuYins)
                    {
                        string[] zuyinStr = CubeUse_ZuYinStr.Match(select.ToString()).Value.Split(new char[] { ',' });
                        if (zuyinStr[4] == "7") break;
                        int zuyinType = int.Parse(zuyinStr[4]);
                        int zuyinWidth = int.Parse(zuyinStr[2]) * 16;
                        int zuyinHeight = int.Parse(zuyinStr[3]) * 16;
                        int zuyinX = int.Parse(zuyinStr[0]) * 16 - 16;
                        int zuyinY = CubeSizeY * 16 + 16 - int.Parse(zuyinStr[1]) * 16 - zuyinHeight;
                        if (type < zuyinType)
                        {
                            type = zuyinType;
                            path[zuyinType].Name = "ZuYin" + zuyinStr[4];
                            WorldPanel.Children.Add(path[zuyinType]);
                            gg = new GeometryGroup();
                            path[zuyinType].Data = gg;
                            path[zuyinType].Fill = new SolidColorBrush(color[zuyinType]);
                            path[zuyinType].StrokeThickness = 0;
                            path[zuyinType].Width = 512;
                            path[zuyinType].Height = 512;
                            path[zuyinType].SetValue(Canvas.LeftProperty, 0.0);
                            path[zuyinType].SetValue(Canvas.BottomProperty, 0.0);
                        }
                        RectangleGeometry r;
                        float scale = CubeSizeMax / 32;
                        r = new RectangleGeometry(new Rect(zuyinX / scale, zuyinY / scale, zuyinWidth / scale, zuyinHeight / scale));
                        gg.Children.Add(r);
                        System.Drawing.Brush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(color[zuyinType].A, color[zuyinType].R, color[zuyinType].G, color[zuyinType].B));
                        g.FillRectangle(brush, (float)(zuyinX * 2 - 0.5), (float)(zuyinY * 2 - 0.5), (float)(zuyinWidth * 2), (float)(zuyinHeight * 2));
                    }
                    for (int j = 0; j < 7; j++)
                    {
                        System.Drawing.Brush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(color[j].A, color[j].R, color[j].G, color[j].B));
                        g.FillRectangle(brush, (float)(CubeSizeX * 32 - 0.5 + 16), (float)(0 + j * 32 - 0.5), (float)(16), (float)(16));
                    }
                    break;
                default:
                    break;
            }
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show(exc.Message);
            //}
        }

        private void CubeItem_Selected(object sender, RoutedEventArgs e)
        {
            FormItem cube = sender as FormItem;
            if (cube == null) return;
            foreach (FormItem select in CubeList.Items)
            {
                if (select.Index > cube.Index)
                {
                    select.RectLink.Visibility = Visibility.Collapsed;
                }
                else
                {
                    select.RectLink.Visibility = Visibility.Visible;
                }
            }
        }

        private void SaveToImage(FrameworkElement ui, string fileName)
        {

            System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create);
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)ui.ActualHeight, (int)ui.ActualWidth, graphics.DpiX, graphics.DpiY, PixelFormats.Pbgra32);
            bmp.Render(ui);
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            encoder.Save(fs);
            fs.Close();
        }
        int FileIndex = 0;
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //SaveToImage(WorldPanel, "1.png");
            try
            {
                string savePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\" + PictureName.Text + FileIndex.ToString() + ".png";
                if (System.IO.File.Exists(savePath))
                {
                    if (MessageBox.Show("图片：" + savePath + "已经存储，是否覆盖？", "覆盖图片", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    {
                        return;
                    }
                }
                FileIndex++;
                bitmap.Save(savePath, System.Drawing.Imaging.ImageFormat.Png);
                Script.Text = "";
            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }
    }
}
